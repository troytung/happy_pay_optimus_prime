
### todo: 改成批次執行FTP上傳作業

### 產出次特店資料並上傳中信FTP

* 執行 CTBCPrintTest.java 程式產出 ctbcmsub.txt，記得去掉結尾換行符號

* 壓縮成 ctbcmsub.zip，壓縮加密密碼：CTBC822
~~~bash
zip -e ctbcmsub.zip ctbcmsub.txt
~~~

* 開啟VPN連線到支付樂主機(202.173.54.23)

* 上傳 ctbcmsub.zip 檔案到主機
~~~bash
cd <path to happy_pay_optimus_prime>
ping 10.20.0.2
chmod go-rw hap.pem
scp -i hap.pem -P 44919 ctbcmsub.zip happypay@10.20.0.2:~/
~~~

* 上傳檔案到中信 FTP server
~~~bash
ssh -i hap.pem -p 44919 happypay@10.20.0.2
$ftp
ftp> open 203.66.193.20
happy_acq@tradevanftp@203.66.193.20
ja29ix6k@traftp01
ftp> passive
ftp> put ctbcmsub.zip
ftp> ls
ftp> bye
~~~

* Issues solved  
    Q: Cannot connect to 10.20.0.2  
    A: Add route table:  
    $ sudo route add -net 10.20.0.0/16 192.168.210.254

    Q: Cannot connect to ftp server  
    A: make sure your external IP is 202.173.54.23



package com.happypay.optimus;

import org.junit.Test;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by HappyPay on 18/07/2017.
 */
public class HttpClientTest {

    @Test
    public void testHttpsSocket() throws Exception {
//        String url = "https://wwwtest.einvoice.nat.gov.tw/APMEMBERVAN/membercardlogin?card_ban=52692665&card_no1=111&card_no2=222&card_type=test&back_url=https%3A%2F%2Feinvoice.happy-pay.com%2Ftoken&token=asdfer13333";
        String url = "https://einvoice.happy-pay.com/membercard?card_ban=52692665&card_no1=111&card_no2=222&card_type=test&back_url=https%3A%2F%2Feinvoice.happy-pay.com%2Ftoken&token=asdfer13333&user_id=123";
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "I am Optimus");
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());
    }
}

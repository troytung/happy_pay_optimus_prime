package com.happypay.optimus;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by HappyPay on 07/07/2017.
 */
public class LoadFileTest {

    @Test
    public void testLoadFileAndFormatToSQL() throws IOException {

//        Map<String, String> bankCodeMap = new HashMap<>();
//        Files.readAllLines(Paths.get(getClass().getResource("/bank_list").getPath())).parallelStream().forEach(l -> {
//            String[] code = l.split(",");
//            bankCodeMap.put(code[1], code[0]);
//        });
        List<String> bankTable = Files.readAllLines(Paths.get(getClass().getResource("/bank_bin_table").getPath()));
        bankTable.forEach(r -> {
            final String sql = "INSERT INTO card_issuer_info(bin_code, name, bank_code, brand, created_time, updated_time, deleted, version)" +
                    " VALUES('$binCode', '$name', '$bankCode', '$brand', $t, $t, false, 0)";

            final String[] record = r.split(",");
            final String result = sql.replace("$binCode", record[0]).replace("$name", record[1])
                    .replace("$bankCode", record[2])
                    .replace("$brand", record[3]).replaceAll("\\$t", String.valueOf(System.currentTimeMillis()));
            System.out.println(result);
        });
    }

    enum CardBrand {
        OTHERS(0),
        JCB(3),
        VISA(4),
        MASTER(5);

        private int code;

        private CardBrand(int code) {
            this.code = code;
        }

        public int getCode() {
            return this.code;
        }

        public static Optional<CardBrand> getBrand(int code) {
            return Arrays.stream(CardBrand.values()).filter(c -> c.getCode() == code).findFirst();
        }
    }
}

package com.happypay.optimus;

import org.junit.Test;

import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class CTBCPrintTest {

    @Test
    public void printSubMerchantMasterFile() throws IOException {
        List<String> lines = Arrays.asList(this.subMerchantMasterFile_cheerful(), this.subMerchantMasterFile_evatar());
//        lines.forEach(System.out::println);
        Path file = Paths.get("/Users/troytung/happy/hap_repo/happy_pay_optimus_prime/ctbcmsub.txt");
        Files.write(file, lines, Charset.forName("BIG5"));
    }

    public String subMerchantMasterFile_cheerful() throws UnsupportedEncodingException {
        String merchantId = padNumber(822013330385L, 12);
        String subMerchantId = padString("CHEERFUL001", 15);
        String pfi = padNumber(52692655, 11);
        String subMerchantNameEnglish = padString("HAPPYPAY----*CHEERFUL", 22);
        String subMerchantAddress = padString("1F., No.18, Aly. 1, Ln. 768, Sec. 4, Bade Rd.", 48);
        String subMerchantCity = padString("Taipei City", 13);
        String subMerchantCountry = padString("TWN", 3);
        String subMerchantPostal = padString("105", 10);
        String mccCode = padString("5399", 4);
        String reservedField1 = padString("", 70);
        String subMerchantNameChinese = padFullWidthString("台灣吉而好", 11);//文件定義長度 22
        String storeType = padString("1", 1);
        String productType = padString("1", 1);
        String transactionType = padString("1", 1);
        String mainProduct = padFullWidthString("百貨", 20);//文件定義長度 40
        String prepaidPeriod = padString("00", 2);
        String guaranteeType = padString("00", 2);
        String guaranteeBank = padString("NA", 20);
        String guaranteePeriod = padNumber(0, 8);
        String registryName = padFullWidthString("台灣吉而好股份有限公司", 20);//文件定義長度 40
        String businessName = padFullWidthString("台灣吉而好", 20);//文件定義長度 40
        String openDate = padNumber(20170807, 8);
        String website = padString("http://www.cheerful.com.tw/", 120);
        String verificationMethod = padString("3", 1);
        String riskFlag = padString("1", 1);
        String prepaidFundingFlag = padString("0", 1);
        String fundingDays = padNumber(2, 3);
        String transactionLimit = padNumber(3000000L, 11);
        String fundingLimit = padNumber(99999999999L, 11);
        String perTransactionLimit = padNumber(99999, 11);
        String cancelFlag = padString("1", 1);
        String violationFlag = padString("00", 2);
        String violationControlFlag = padString("00", 2);
        String corporateId = padString("21245516", 10);
        String reservedField2 = padString("", 282);
        String endOfRecordMark = padString("!", 1);

//        PrintStream out = new PrintStream(System.out, true, "BIG5");
        return merchantId + subMerchantId + pfi + subMerchantNameEnglish + subMerchantAddress + subMerchantCity +
                subMerchantCountry + subMerchantPostal + mccCode + reservedField1 + subMerchantNameChinese + storeType +
                productType + transactionType + mainProduct + prepaidPeriod + guaranteeType + guaranteeBank + guaranteePeriod +
                registryName + businessName + openDate + website + verificationMethod + riskFlag + prepaidFundingFlag +
                fundingDays + transactionLimit + fundingLimit + perTransactionLimit + cancelFlag + violationFlag + violationControlFlag +
                corporateId + reservedField2 + endOfRecordMark;
    }

    public String subMerchantMasterFile_evatar() throws UnsupportedEncodingException {
        String merchantId = padNumber(822013330022L, 12);
        String subMerchantId = padString("EVATAR001", 15);
        String pfi = padNumber(52692655, 11);
        String subMerchantNameEnglish = padString("HAPPYPAY----*EVATAR", 22);
        String subMerchantAddress = padString("4F.-1, No.35, Guangfu S. Rd., Songshan Dist.", 48);
        String subMerchantCity = padString("Taipei City", 13);
        String subMerchantCountry = padString("TWN", 3);
        String subMerchantPostal = padString("105", 10);
        String mccCode = padString("5816", 4);
        String reservedField1 = padString("", 70);
        String subMerchantNameChinese = padFullWidthString("支付樂－伊凡達", 11);
        String storeType = padString("1", 1);
        String productType = padString("2", 1);
        String transactionType = padString("5", 1);
        String mainProduct = padFullWidthString("伊凡達－遊戲", 20);
        String prepaidPeriod = padString("00", 2);
        String guaranteeType = padString("00", 2);
        String guaranteeBank = padString("NA", 20);
        String guaranteePeriod = padNumber(0, 8);
        String registryName = padFullWidthString("伊凡達科技股份有限公司", 20);
        String businessName = padFullWidthString("伊凡達遊戲", 20);
        String openDate = padNumber(20170715, 8);
        String website = padString("http://www.8888play.com", 120);
        String verificationMethod = padString("3", 1);
        String riskFlag = padString("1", 1);
        String prepaidFundingFlag = padString("0", 1);
        String fundingDays = padNumber(7, 3);
        String transactionLimit = padNumber(99999999999L, 11);
        String fundingLimit = padNumber(99999999999L, 11);
        String perTransactionLimit = padNumber(99999999999L, 11);
        String cancelFlag = padString("1", 1);
        String violationFlag = padString("00", 2);
        String violationControlFlag = padString("00", 2);
        String corporateId = padString("54140696", 10);
        String reservedField2 = padString("", 282);
        String endOfRecordMark = padString("!", 1);

        PrintStream out = new PrintStream(System.out, true, "BIG5");
        return merchantId + subMerchantId + pfi + subMerchantNameEnglish + subMerchantAddress + subMerchantCity +
                subMerchantCountry + subMerchantPostal + mccCode + reservedField1 + subMerchantNameChinese + storeType +
                productType + transactionType + mainProduct + prepaidPeriod + guaranteeType + guaranteeBank + guaranteePeriod +
                registryName + businessName + openDate + website + verificationMethod + riskFlag + prepaidFundingFlag +
                fundingDays + transactionLimit + fundingLimit + perTransactionLimit + cancelFlag + violationFlag + violationControlFlag +
                corporateId + reservedField2 + endOfRecordMark;
    }

    public String subMerchantMasterFile_tradevan() throws UnsupportedEncodingException {
        String merchantId = padNumber(822013330022L, 12);
        String subMerchantId = padString("TRADEVAN001", 15);
        String pfi = padNumber(52692655, 11);
        String subMerchantNameEnglish = padString("HAPPYPAY----*TRADEVAN", 22);
        String subMerchantAddress = padString("No. 19-13, Sanchong Road, Nangang District", 48);
        String subMerchantCity = padString("Taipei City", 13);
        String subMerchantCountry = padString("TWN", 3);
        String subMerchantPostal = padString("115", 10);
        String mccCode = padString("7399", 4);
        String reservedField1 = padString("", 70);
        String subMerchantNameChinese = padString("支付樂－關貿網路", 22).replace(" ".charAt(0), (char) 12288);
        String storeType = padString("1", 1);
        String productType = padString("2", 1);
        String transactionType = padString("2", 1);
        String mainProduct = padString("提供資訊系統整合服務", 40).replace(" ".charAt(0), (char) 12288);
        String prepaidPeriod = padString("00", 2);
        String guaranteeType = padString("00", 2);
        String guaranteeBank = padString("NA", 20);
        String guaranteePeriod = padNumber(20201231, 8);
        String registryName = padString("關＊網路股份有限公司", 40).replace(" ".charAt(0), (char) 12288);
        String businessName = padString("關＊貿網路", 40).replace(" ".charAt(0), (char) 12288);
        String openDate = padNumber(20160515, 8);
        String website = padString("http://www.tradevan.com.tw/", 120);
        String verificationMethod = padString("3", 1);
        String riskFlag = padString("1", 1);
        String prepaidFundingFlag = padString("0", 1);
        String fundingDays = padNumber(30, 3);
        String transactionLimit = padNumber(99999999999L, 11);
        String fundingLimit = padNumber(666666666, 11);
        String perTransactionLimit = padNumber(6666, 11);
        String cancelFlag = padString("2", 1);
        String violationFlag = padString("00", 2);
        String violationControlFlag = padString("00", 2);
        String corporateId = padString("97162640", 10);
        String reservedField2 = padString("", 282);
        String endOfRecordMark = padString("!", 1);

        PrintStream out = new PrintStream(System.out, true, "BIG5");
        return merchantId + subMerchantId + pfi + subMerchantNameEnglish + subMerchantAddress + subMerchantCity +
                subMerchantCountry + subMerchantPostal + mccCode + reservedField1 + subMerchantNameChinese + storeType +
                productType + transactionType + mainProduct + prepaidPeriod + guaranteeType + guaranteeBank + guaranteePeriod +
                registryName + businessName + openDate + website + verificationMethod + riskFlag + prepaidFundingFlag +
                fundingDays + transactionLimit + fundingLimit + perTransactionLimit + cancelFlag + violationFlag + violationControlFlag +
                corporateId + reservedField2 + endOfRecordMark;
    }

//    @Test
//    public void transactionFileHeader() {
//        // ------------ header ------------- //
//        String recordType = padString("", 1);
//        String processingDate = padNumber("", 6);
//        String merchantNumber = padNumber("", 9);
//        String merchatNumberCheckDigit = padNumber("", 1);
//        String tranSactionCount_sale = padNumber("", 8);
//        String totalAmount_sale = padNumber("", 10);
//        String transactionCount_refund = padNumber("", 8);
//        String totalAmout_refund = padNumber("", 10);
//        String balanceCode = padString("", 1);
//        String transactionDescription = padString("", 40);
//        String balanceCode_sale = padString("", 1);
//        String balanceCode_refund = padString("", 1);
//        String reservedField = padString("", 4);
//        String reservedField = padString("", 98);
//    }

//    public void transactionFileDetail() {
//        String recordType = padString("", 1);
//        String transactionDate = padNumber("", 6);
//        String creditCardNumber = padString("", 16);
//        String creditCardNumberExtension = padString("", 3);
//        String transactionCode = padNumber("", 2);
//        String authorizationCode = padString("", 6);
//        String transactionAmount = padNumber("", 10);
//        /**
//         * 全形-填滿23個bype
//         * 半形-填滿25個byte
//         * 全形字(包括空白)為主, 無法提供再放置半形.
//         */
//        String transactionDescription = padString("", 23);
//        String securityIndicator = padString("", 1);
//        String processingResponseCode = padString("", 1);
//        String terminalId = padString("", 8);
//        String gatewayReservedField = padString("", 11);
//        String reservedField = padString("", 1);
//        String particalShipmentOrrecurringIndicator = padString("", 1);
//        String ecWalletTransactionIndicator = padString("", 1);
//        String mastercardTerminalOperatingEnvironment = padString("", 1);
//        String posEntryMode = padString("", 3);
//        String mastercardEcSecurityLevelIndicator_securityProtocol = padString("", 1);
//        String mastercardEcSecurityLevelIndicator_cardhoderAuthentication = padString("", 1);
//        String mastercardEcSecurityLevelIndicator_mastercardUCAF = padString("", 1);
//        String subMerchantId = padString("", 15);
//        String dccSourceCode = padString("", 3);
//        String dccCurrencyCode = padString("", 3);
//        String dccConversionRate = padNumber("", 7); // 3位整數4位小數, 如32.1記為0321000, 9(3)V9(4)
//        String dccCurrencyAmount = padNumber("", 11); // 9位整數2位小數, 如32.1記為00000003210, 9(9)V9(2)
//        String reservedField = padString("", 19);
//        String transactionProductName = padString("", 40);
//    }
//
//    public void transactionFileTail() {
//        String recordType = padString("9", 1);
//        String processingDate = padNumber(170626,6);
//        String recordCount = padNumber(2,8);
//        String filler = padString("",183);
//    }

    private String padString(String s, int n) {
        return String.format("%1$-" + n + "s", new String(s.getBytes(), Charset.defaultCharset()));
    }

    private String padFullWidthString(String s, int n) {
        return padString(s, n).replace(" ".charAt(0), (char) 12288);
    }

    private String padNumber(Long l, int n) {
        return String.format("%0" + n + "d", l);
    }

    private String padNumber(Integer l, int n) {
        return String.format("%0" + n + "d", l);
    }

}